// ** Paths to CSS libs
// ** Build in _libs.css and send it to src/sass/root folder
var cssLibs = [ 
	"./node_modules/normalize.css/normalize.css",
	"./node_modules/@splidejs/splide/dist/css/splide.min.css",
	"./node_modules/bootstrap-4-grid/css/grid.css"
]

// ** Modules which you import from node_modules
var nodeModules = {
	modules: {
		$: "jquery",
		'jQuery': 'jquery',
		'windows.jQuery': 'jquery',
	}
}

module.exports = {
	cssLibs,
	nodeModules
}
