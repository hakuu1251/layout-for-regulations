import Splide from "@splidejs/splide"

export default function slider() {
	const mainSlider = new Splide("#main-slider", {
		type: "loop",
		autoplay: true,
		lazyLoad: true,
		pagination: false,
	})
	mainSlider.mount()

	const productSlider = new Splide("#product-slider", {
		type: "loop",
		perPage: 4,
		perMove: 1,
		width: "100%",
		autoplay: true,
		pauseOnFocus: true,
		lazyLoad: true,
		pagination: false,
		padding: { top: 10, bottom: 20 },
		breakpoints: {
			1200: {
				perPage: 3,
			},
			992: {
				perPage: 2,
			},
			768: {
				perPage: 1,
				width: "100%",
				height: "100%",
			},
		},
	})
	productSlider.mount()
}
