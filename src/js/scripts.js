import $ from "jquery"
window.jQuery = $
import "jquery.maskedinput/src/jquery.maskedinput"
import "./_jquery.validate.min"
import slider from "./_slider"
import upload from './_upload'
import modal from "./_modal"

function modals() {
	const myModal = modal()
	$("#open-modal").click(function () {
		myModal.open()
	})
}

$(document).ready(function () {
	slider()
	upload()
	modals()
})
