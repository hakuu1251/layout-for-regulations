export default $.modal = function (options) {
	jQuery.extend(jQuery.validator.messages, {
		required: "Это поле обязательно.",
		max: jQuery.validator.format("Максимальное количество символов {0}."),
		min: jQuery.validator.format("Минимальное количество символов {0}."),
	})

	jQuery(function ($) {
		$('input[name="phone"]').mask("+7(999)-999-99-99")
		$("#auth-form").validate({
			ignore: ".ignore",
			rules: {
				phone: {
					required: true,
				},
				pass: {
					required: true,
					minlength: 6,
				},
				email: {
					required: true,
					email: true,
				},
				accept: {
					required: true,
				},
			},
		})
	})

	const $modal = $(".modal")
	let destroyed = false

	const methods = {
		open() {
			$modal.addClass("modal-open")
			$(document.body).css({ "overflow-y": "hidden" })
		},
		close() {
			$modal.removeClass("modal-open")
			$(document.body).css({ "overflow-y": "auto" })
		},
		destroy() {},
	}

	$modal.click(function (event) {
		if (event.target.dataset.close) {
			methods.close()
		}
	})

	return Object.assign(methods, {
		destroy() {
			$modal.parent().remove($modal)
			$modal.off()
			destroyed = true
		},
	})
}
